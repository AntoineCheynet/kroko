module.exports = {
  //preset: "@vue/cli-plugin-unit-jest",
  testRegex: "./tests/*/.*.spec.js$",
  moduleFileExtensions: ["js", "json", "vue"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1"
  },
  transform: {
    "^.+\\.js$": "<rootDir>/node_modules/babel-jest",
    ".*\\.(vue)$": "<rootDir>/node_modules/vue-jest",
    "^.+\\.vue$": "vue-jest"
  }
};
