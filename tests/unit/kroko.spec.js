import Kroko from "@/components/Kroko";
import KrokoJaw from "@/components/kroko/KrokoJaw";
import { shallowMount } from "@vue/test-utils";
jest.mock("../../src/services/randomize-tooth.js");

describe("Kroko", function() {
  const TRIGGER = 4;
  let wrapper;

  async function clickOn(index) {
    wrapper.findComponent(KrokoJaw).vm.$emit("tooth-clicked", index + 1);
    await wrapper.vm.$nextTick();
  }

  function makeWrapper(numberTeeth) {
    wrapper = shallowMount(Kroko, {
      props: {
        numberTeeth
      }
    });
  }

  it("renders the number of tries left", () => {
    makeWrapper(2);

    expect(wrapper.text()).toContain("tries remaining: 2");
  });

  it("decreases its counter each time a tooth is clicked", async () => {
    makeWrapper(2);

    await clickOn(1);
    expect(wrapper.text()).toContain("try remaining: 1");

    await clickOn(2);
    expect(wrapper.text()).toContain("tries remaining: 0");
  });

  it("cannot go below the number of teeth", async () => {
    makeWrapper(1);

    await clickOn(0);
    expect(wrapper.text()).toContain("tries remaining: 0");

    await clickOn(1);
    expect(wrapper.text()).toContain("tries remaining: 0");
  });

  it("cannot have the same tooth clicked twice", async () => {
    makeWrapper(10);

    await clickOn(0);
    expect(wrapper.text()).toContain("tries remaining: 9");

    await clickOn(0);
    expect(wrapper.text()).toContain("tries remaining: 9");
  });

  it("stops when the player click on the trigger tooth", async () => {
    makeWrapper(10);

    await clickOn(TRIGGER);

    expect(wrapper.text()).toContain("tries remaining: 0");
  });

  it("emits the right event when the answer is no", async () => {
    makeWrapper(10);

    await clickOn(TRIGGER);

    expect(wrapper.emitted().gameover[0]).toEqual([{ tries: 9, answer: "no" }]);
  });

  it("emits the right event when the answer is yes", async () => {
    makeWrapper(2);

    await clickOn(0);
    await clickOn(1);

    expect(wrapper.emitted().gameover[0]).toEqual([
      { tries: 2, answer: "yes" }
    ]);
  });
});
