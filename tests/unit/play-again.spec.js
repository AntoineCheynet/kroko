import { mount } from "@vue/test-utils";
import PlayAgain from "@/components/PlayAgain";

describe("PlayAgain", function() {
  it("can call for a new game", async () => {
    const wrapper = mount(PlayAgain, {
      props: {
        answer: "no"
      }
    });

    await wrapper.find("#play-again").trigger("click");

    expect(wrapper.emitted("play-again")[0]).toEqual([true]);
  });

  it("can quit the game", async () => {
    const wrapper = mount(PlayAgain, {
      props: {
        answer: "no"
      }
    });

    await wrapper.find("#quit-game").trigger("click");

    expect(wrapper.emitted("play-again")[0]).toEqual([false]);
  });
});
