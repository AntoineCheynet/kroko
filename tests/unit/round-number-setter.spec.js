import { shallowMount } from "@vue/test-utils";
import RoundNumberSetter from "@/components/RoundNumberSetter.vue";

describe("RoundNumberSetter", function() {
  let wrapper;
  let playButton;
  let decreaseButton;
  let increaseButton;

  beforeEach(() => {
    wrapper = shallowMount(RoundNumberSetter);
    playButton = wrapper.find("#play");
    decreaseButton = wrapper.find("#number-round-decrease");
    increaseButton = wrapper.find("#number-round-increase");
  });

  it("can increase the number of teeth for a game", async () => {
    await increaseButton.trigger("click");
    await playButton.trigger("click");

    expect(wrapper.emitted().play[0]).toEqual([2]);
  });

  it("cannot decrease below 1", async () => {
    for (let i = 0; i < 15; i++) {
      await decreaseButton.trigger("click");
    }
    await playButton.trigger("click");

    expect(wrapper.emitted().play[0]).toEqual([1]);
  });

  it("cannot increase above 10", async () => {
    for (let i = 0; i < 15; i++) {
      await increaseButton.trigger("click");
    }
    await playButton.trigger("click");

    expect(wrapper.emitted().play.length).toBe(1);
    expect(wrapper.emitted().play[0]).toEqual([10]);
  });
});
