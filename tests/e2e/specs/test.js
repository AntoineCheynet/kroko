import { randomizeTooth } from "../../../src/services/randomize-tooth.js";

describe("Kroko", () => {
  it("shows the player's choice", () => {
    cy.visit("/");
    cy.get("button#play").click();
    cy.get("body").contains("Number of teeth: 1");
    cy.get("body").contains("try remaining: 1");
  });

  it("selects the number of teeth for a game", () => {
    cy.visit("/");
    cy.get("#number-round-increase").click();
    cy.get("body").contains("2");
    cy.get("#number-round-increase").click();
    cy.get("body").contains("3");
    cy.get("#number-round-decrease").click();
    cy.get("body").contains("2");
    cy.get("button#play").click();
    cy.get("body").contains("Number of teeth: 2");
    cy.get("body").contains("tries remaining: 2");
  });

  it("answers no when the player clicks on the wrong tooth", () => {
    cy.stub(randomizeTooth, "get").returns(1);
    cy.visit("/");
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("#number-round-increase").click();
    cy.get("body").contains("10");
    cy.get("button#play").click();
    cy.get("#tooth-3").click();
    cy.get("#tooth-2").click();
    cy.get("#tooth-1").click();
    cy.get("body").contains("Play Again");
  });
});
