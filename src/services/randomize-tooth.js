const randomizeTooth = {
  get: () => Math.floor(Math.random() * Math.floor(9)) + 1
};

export { randomizeTooth };
