import "@/assets/css/style.css";
import { createApp } from "vue";
import App from "./App.vue";

window.__VUE__ = true;

createApp(App).mount("#app");
